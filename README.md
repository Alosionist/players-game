# players-game

- TODO: better README

this project includes:
- nestjs server
- angular client

## how to use:
### install:
- nodejs
- typescript:
```
npm install -g typescript
```
- angular
```
npm install -g @angular/cli
```
- nestjs:
```
npm i -g @nestjs/cli
```

### run dev:
- run server on port 3000:
```
$ npm install
$ npm run start:dev
```
- run client on port 4200:
```
$ npm install
$ ng serve --port 4200
```
add `--host 0.0.0.0` to use in local network
