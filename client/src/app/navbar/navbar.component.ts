
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FaqComponent } from '../faq/faq.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  @Output() restart = new EventEmitter<void>();

  constructor(public dialog: MatDialog) {}

  ngOnInit(): void {}

  emitRestart(): void {
    this.restart.emit();
  }

  openFAQ() {
    this.dialog.open(FaqComponent, { width: '580px',  minHeight: '90%', maxWidth: '90%', maxHeight: '95%'});
  }
}
