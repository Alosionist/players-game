import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Player } from '../models/player';
import { GameService } from '../game.service';
import { Output, EventEmitter } from '@angular/core';
import { GuessResponse } from '../models/guessResponse';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent implements OnInit {
  myControl = new FormControl();
  filteredOptions$: Observable<Player[]>;
  @Input() players: Player[];
  @Input() guessCount: number;
  @Output() guess = new EventEmitter<GuessResponse>();

  constructor(private gameService: GameService) {}

  ngOnInit() {
    this.filteredOptions$ = this.myControl.valueChanges.pipe(
      startWith(''),
      map((value: string) => this._filter(value))
    );
  }

  displayFn(player: Player): string {
    return player?.name;
  }

  select(): void {
    this.gameService
      .guess(this.myControl.value)
      .subscribe((guessResponse: GuessResponse) => {
        this.guess.emit(guessResponse);
      });
    this.myControl.patchValue({});
  }

  private _filter(value: string): Player[] {
    if (typeof value !== 'string') return [];
    if (value.length < 2) return [];
    const filterValue = value.toLowerCase();

    return this.players.filter((player) =>
      player.name.toLowerCase().includes(filterValue)
    );
  }
}
