import { Component, Input, OnInit } from '@angular/core';
import { GameResult } from '../models/gameResult';
import { Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'app-player-box',
  templateUrl: './player-box.component.html',
  styleUrls: ['./player-box.component.css'],
})
export class PlayerBoxComponent implements OnInit {
  @Input() gameResult: GameResult;
  @Input() count: number;
  @Output() reload = new EventEmitter<void>();

  constructor() {}

  ngOnInit(): void {}

  reloadImage(event: any): void {
    this.reload.emit();
  }
}
