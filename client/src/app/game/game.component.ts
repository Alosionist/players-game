import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { GameService } from '../game.service';
import { GameResult } from '../models/gameResult';
import { GuessResponse } from '../models/guessResponse';
import { GuessResult } from '../models/guessResult';
import { Player } from '../models/player';
import { PlayersService } from '../players.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css'],
})
export class GameComponent implements OnInit {
  private readonly PLAYERS_IMG = 'https://media.api-sports.io/football/players';
  players$: Observable<Player[]>;
  guessResults: GuessResult[] = [];
  gameResult: GameResult = { isActive: true };
  count: number = 0;

  constructor(
    private gameService: GameService,
    private playersService: PlayersService
  ) {}

  ngOnInit(): void {
    this.gameService.init().subscribe((guessResponse: GuessResponse) => {
      if (guessResponse) {
        this.handleFinishGame(guessResponse);
      }
    });
    this.players$ = this.playersService.allPlayers$;
    this.gameService.allGuesses$.subscribe((results: GuessResult[]) => {
      this.guessResults = results.reverse();
    });
  }

  restart(event: any): void {
    this.gameService
      .clear()
      .pipe(switchMap(() => this.gameService.init()))
      .subscribe(() => {
        this.gameResult.isActive = true;
        this.guessResults = [];
        this.reloadImage();
      });
  }

  addGuess(guessResponse: GuessResponse): void {
    this.handleFinishGame(guessResponse);
    this.guessResults.unshift(guessResponse.result);
    this.reloadImage();
  }

  reloadImage(event: any = {}): void {
    setTimeout(() => this.count++, 1);
  }

  private handleFinishGame(guessResponse: GuessResponse) {
    if (guessResponse.answer || guessResponse.result.id.result) {
      this.gameResult = {
        isActive: false,
        name: guessResponse.answer.name,
        win: !!guessResponse.result.id.result,
      };
    }
  }
}
