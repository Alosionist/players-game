import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Player } from './models/player';
import { GuessResponse } from './models/guessResponse';
import { HttpHeaders } from '@angular/common/http';
import { GuessResult } from './models/guessResult';

@Injectable({
  providedIn: 'root',
})
export class GameService {
  private readonly ROUTE = '/game';
  private readonly GUESS_ROUTE = this.ROUTE + '/guess';
  private readonly GUESSES_ROUTE = this.ROUTE + '/guesses';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  guess(player: Player): Observable<GuessResponse> {
    return this.http.post<GuessResponse>(
      this.GUESS_ROUTE,
      player,
      this.httpOptions
    );
  }

  clear(): Observable<void> {
    return this.http.delete<void>(this.ROUTE);
  }

  init(): Observable<GuessResponse> {
    return this.http.post<GuessResponse>(this.ROUTE, {});
  }

  get allGuesses$(): Observable<GuessResult[]> {
    return this.http.get<GuessResult[]>(this.GUESSES_ROUTE);
  }
}
