import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Player } from './models/player';
import { share } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class PlayersService {
  private readonly ROUTE = '/players';

  constructor(private http: HttpClient) {}

  get allPlayers$(): Observable<Player[]> {
    return this.http.get<Player[]>(this.ROUTE).pipe(share());
  }

  getById(id: number): Observable<Player> {
    return this.http.get<Player>(`${this.ROUTE}/${id}`);
  }
}
