import { Component, OnInit } from '@angular/core';
import { AgeHint } from '../models/ageHint';
import { GuessResult } from '../models/guessResult';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css'],
})
export class FaqComponent implements OnInit {
  exampleResult: GuessResult;
  constructor() {}

  ngOnInit(): void {
    this.exampleResult = this.createExamplePlayer();
  }

  createExamplePlayer(): GuessResult {
    return {
      age: { value: 30, result: AgeHint.BELOW },
      leagueId: { value: 39, result: true },
      teamId: { value: 49, result: false },
      nationality: { value: 'Italy', result: false },
      position: { value: 'MF', result: true },
    };
  }
}
