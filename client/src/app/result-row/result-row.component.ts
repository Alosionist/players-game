import { Component, Input, OnInit } from '@angular/core';
import { Player } from '../models/player';
import { GuessResult } from '../models/guessResult';
import { AgeHint } from '../models/ageHint';
import { AssetsService } from '../assets.service';
import { Nationality } from '../models/nationality';

@Component({
  selector: 'app-result-row',
  templateUrl: './result-row.component.html',
  styleUrls: ['./result-row.component.css'],
})
export class ResultRowComponent implements OnInit {
  private readonly NATIONALITY_IMG_PATH =
    'https://missing11.com/who-are-ya/nations';
  private readonly LEAGUE_IMG_PATH =
    'https://media.api-sports.io/football/leagues';
  private readonly TEAM_IMG_PATH = 'https://media.api-sports.io/football/teams';
  @Input() guessResult: GuessResult;
  @Input() small: boolean;
  nationalities: Nationality[];
  isAgeCorrect: boolean;
  nationalityImg: string;
  leagueImg: string;
  teamImg: string;
  arrow: string = '';

  constructor(private assetsService: AssetsService) {}

  ngOnInit(): void {
    this.isAgeCorrect = this.guessResult.age.result === AgeHint.CORRECT;

    this.nationalities = this.assetsService.nationalities;
    const nationalityImgId = this.getByName(this.guessResult.nationality.value);
    this.nationalityImg = `${this.NATIONALITY_IMG_PATH}/${nationalityImgId}.jpg`;

    this.leagueImg = `${this.LEAGUE_IMG_PATH}/${this.guessResult.leagueId.value}.png`;
    this.teamImg = `${this.TEAM_IMG_PATH}/${this.guessResult.teamId.value}.png`;
    if (this.guessResult.age.result === AgeHint.ABOVE) {
      this.arrow = '↑';
    } else if (this.guessResult.age.result === AgeHint.BELOW) {
      this.arrow = '↓';
    }
  }

  getByName(name: string): number {
    return this.nationalities.find((nat: Nationality) => nat.name == name)?.id;
  }
}
