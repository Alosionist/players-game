import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Nationality } from './models/nationality';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AssetsService {
  private readonly ASSETS_PATH = './assets';
  private readonly NATIONALITIES_PATH = `${this.ASSETS_PATH}/nationalities.json`;
  private nats: Nationality[];

  constructor(private http: HttpClient) {}

  load(): Promise<Nationality[]> {
    return this.http
      .get<Nationality[]>(this.NATIONALITIES_PATH)
      .pipe(
        tap((nats) => {
          this.nats = nats;
        })
      )
      .toPromise();
  }

  get nationalities(): Nationality[] {
    return this.nats;
  }
}
