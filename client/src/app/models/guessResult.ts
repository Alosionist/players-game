import { ResultProp } from './resultProp';

export interface GuessResult {
  name?: string;
  id?: ResultProp<number>;
  age?: ResultProp<number>;
  nationality?: ResultProp<string>;
  leagueId?: ResultProp<number>;
  teamId?: ResultProp<number>;
  position?: ResultProp<string>;
}
