export interface GameResult {
  name?: string;
  isActive?: boolean;
  win?: boolean;
}
