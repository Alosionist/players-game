export interface Player {
  id: number;
  name: string;
  birthdate: string;
  nationality: string;
  leagueId: number;
  teamId: number;
  position: string;
}
