import { Player } from './player';
import { GuessResult } from './guessResult';

export interface GuessResponse {
  result: GuessResult;
  answer?: Player;
}
