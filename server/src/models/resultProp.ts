import { AgeHint } from "./ageHint";

export interface ResultProp<T> {
  value: T;
  result: boolean | AgeHint;
}