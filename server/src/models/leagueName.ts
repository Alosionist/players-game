export enum leagueNames {
  LEAGUE_1       = "61",
  LALIGA         = "140",
  BUNDESLIGA     = "78",
  SERIE_A        = "135",
  PREMIER_LEAGUE = "39"
}