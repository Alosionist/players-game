export enum AgeHint {
    BELOW = 'BELOW',
    CORRECT = 'CORRECT',
    ABOVE = 'ABOVE'
}