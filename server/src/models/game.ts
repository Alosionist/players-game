import { GuessResult } from "./guessResult";
import { Player } from "./player";

export interface Game {
  userId: string;
  answer: Player;
  guesses: GuessResult[];
  isImageInProcess: boolean;
}