import { Controller, Get, Param, NotFoundException } from '@nestjs/common';
import { DbService } from './db-service/db.service';
import { Player } from './models/player';

@Controller('players')
export class AppController {
  constructor(private readonly dbService: DbService) {}

  @Get(':id')
  getPlayerById(@Param() { id }): Player {
    console.log('GET /players/' + id);
    const player: Player = this.dbService.getPlayerById(id);
    if (player) {
      return player;
    } else {
      throw new NotFoundException('Player Not Found');
    }
  }

  @Get()
  complete(): Player[] {
    console.log('GET /players');

    return this.dbService.getAll();
  }
}
