import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Post,
  Req,
  Res,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { Md5 } from 'ts-md5/dist/md5';
import { GameService } from './game.service';
import { GuessResponse } from 'src/models/guessResponse';
import { GuessResult } from 'src/models/guessResult';

@Controller('game')
export class GameController {
  private readonly DAY: number = 1000 * 3600 * 24 * 7;
  constructor(private readonly gameService: GameService) {}

  @Get('image/:id')
  async getImage(@Req() request: Request, @Res() res: Response): Promise<void> {
    console.log('GET /game/image');
    res.sendFile('secret.png', {
      root: `./images/${this.getGameCookie(request)}/`,
    });
  }

  @Get('guesses')
  getAllGuesses(@Req() request: Request): GuessResult[] {
    console.log('GET /game/guesses');
    const userId = this.getGameCookie(request);
    return this.gameService.getAllGuesses(userId);
  }

  @Post('guess')
  async takeGuess(
    @Body() { id },
    @Req() request: Request,
  ): Promise<GuessResponse> {
    const userId = this.getGameCookie(request);
    const result = await this.gameService.guess(parseInt(id), userId);
    if (result) {
      return result;
    } else {
      throw new NotFoundException('player not found');
    }
  }

  @Post()
  async init(
    @Req() request: Request,
    @Res({ passthrough: true }) response: Response,
  ): Promise<GuessResponse> {
    console.log('POST /game - init game');
    const userId = this.handleCookies(request, response);
    return this.gameService.initGame(userId);
  }

  @Delete()
  clear(@Req() request: Request) {
    console.log('DELETE /game');
    this.gameService.clearGame(this.getGameCookie(request));
  }

  private handleCookies(request: Request, response: Response): string {
    const requestCookie = this.getGameCookie(request);
    if (requestCookie) return requestCookie;
    const newCookie = Md5.hashStr(new Date().toLocaleString());
    response.cookie('game', newCookie, {
      expires: new Date(new Date().getTime() + this.DAY),
    });
    return newCookie;
  }

  private getGameCookie(req: Request) {
    return req.cookies['game'];
  }
}
