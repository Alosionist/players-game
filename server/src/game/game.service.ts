import { Injectable } from '@nestjs/common';
import { DbService } from 'src/db-service/db.service';
import { AgeHint } from 'src/models/ageHint';
import { Game } from 'src/models/game';
import { GuessResponse } from 'src/models/guessResponse';
import { Player } from 'src/models/player';
import { GuessResult } from 'src/models/guessResult';
import { ResultProp } from 'src/models/resultProp';
import { BlurService } from 'src/blur/blur.service';

@Injectable()
export class GameService {
  private games: Map<string, Game> = new Map();
  private readonly MAX_GUESSUS: number = 8;

  constructor(
    private readonly dbService: DbService,
    private readonly blurService: BlurService,
  ) {}

  getAllGuesses(userId: string): GuessResult[] {
    return this.games.get(userId)?.guesses ?? [];
  }

  clearGame(userId: string): void {
    this.games.delete(userId);
  }

  async initGame(userId: string): Promise<GuessResponse> {
    if (!this.games.get(userId)) {
      this.games.set(userId, await this.createNewGame(userId));
    } else {
      const game = this.games.get(userId);
      if (game.guesses.length > 0) {
        const guessResult = game.guesses[game.guesses.length - 1];
        return this.createResponse(game, guessResult);
      }
    }
  }

  guess(id: number, userId: string): Promise<GuessResponse> {
    const game = this.games.get(userId);

    const guess = this.dbService.getPlayerById(id);
    if (!guess) return;
    console.log(`user ${userId} guessed ${guess.id}`);

    const guessResult: GuessResult = this.createGuessResult(guess, game.answer);
    game.guesses.push(guessResult);

    return this.createResponse(game, guessResult);
  }

  private getAgeHint(guessAge: number, answerAge: number): AgeHint {
    if (answerAge > guessAge) return AgeHint.ABOVE;
    if (answerAge < guessAge) return AgeHint.BELOW;
    else return AgeHint.CORRECT;
  }

  private calcAge(date: string): number {
    let ageDate = new Date(Date.now() - new Date(date).getTime());
    return Math.abs(ageDate.getUTCFullYear() - 1970);
  }

  private async createNewGame(userId: string): Promise<Game> {
    const answer = this.dbService.getRandomPopularPlayer();
    await this.blurService.createNewImage(userId, answer.id);
    return {
      userId,
      answer,
      guesses: [],
      isImageInProcess: false,
    };
  }

  private createGuessResult(guess: Player, answer: Player): GuessResult {
    return {
      name: guess.name,
      id: this.createGuessProp(guess.id, answer.id),
      leagueId: this.createGuessProp(guess.leagueId, answer.leagueId),
      nationality: this.createGuessProp(guess.nationality, answer.nationality),
      teamId: this.createGuessProp(guess.teamId, answer.teamId),
      position: this.createGuessProp(guess.position, answer.position),
      age: this.createGuessProp(
        this.calcAge(guess.birthdate),
        this.calcAge(answer.birthdate),
        this.getAgeHint,
      ),
    };
  }

  private createGuessProp<T>(
    value: T,
    answerValue: T,
    compareFunc?: (a: T, b: T) => boolean | AgeHint,
  ) {
    const result = compareFunc
      ? compareFunc(value, answerValue)
      : value == answerValue;
    return { value, result } as ResultProp<T>;
  }

  private async createResponse(
    game: Game,
    guessResult: GuessResult,
  ): Promise<GuessResponse> {
    const response: GuessResponse = { result: guessResult };

    if (game.guesses.length >= this.MAX_GUESSUS || guessResult.id.result) {
      response.answer = game.answer;
      await this.blurService.createNewImage(game.userId, game.answer.id, 0);
    } else {
      await this.blurService.createNewImage(
        game.userId,
        game.answer.id,
        this.blurService.calcBlur(game.guesses.length),
      );
    }

    return response;
  }
}
