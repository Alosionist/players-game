import { Injectable } from '@nestjs/common';
import data from './players.json';
import popular from './popular-players.json';
import { Player } from '../models/player';

@Injectable()
export class DbService {
  getPlayerByName(name: string): Player {
    return data.find((p) => p.name.toLowerCase() == name.toLowerCase());
  }

  getPlayerById(id: number): Player {
    return data.find((p) => p.id == id);
  }

  getAll(): Player[] {
    return data;
  }

  getRandomPlayer(): Player {
    const randomNumber = (Math.random() * data.length) | 0;
    console.log('generated secret player: ', data[randomNumber]);

    return data[randomNumber];
  }

  getRandomPopularPlayer(): Player {
    const randomNumber = (Math.random() * popular.length) | 0;
    console.log(
      'generated secret player: \n id: ',popular[randomNumber].id,
       ', name: ', popular[randomNumber].name,);

    return popular[randomNumber];
  }
}
