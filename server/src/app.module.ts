import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DbService } from './db-service/db.service';
import { GameService } from './game/game.service';
import { GameController } from './game/game.controller';
import { BlurService } from './blur/blur.service';

@Module({
  imports: [],
  controllers: [AppController, GameController],
  providers: [AppService, DbService, GameService, BlurService],
})
export class AppModule {}
