import { Injectable } from '@nestjs/common';
import Jimp from 'jimp';

@Injectable()
export class BlurService {
  private readonly BLUR_BASE = 13;

  createNewImage(
    userId: string,
    answerId: number,
    blurFactor: number = this.BLUR_BASE,
  ): Promise<void> {
    console.log(`creating new image for ${userId} with blur of ${blurFactor}`);
    const imgUrl = `https://media.api-sports.io/football/players/${answerId}.png`;
    return this.blur(imgUrl, userId, blurFactor);
  }

  calcBlur(guessNumber: number): number {
    return this.BLUR_BASE - Math.floor(guessNumber / 1.2);
  }

  private blur(
    imgUrl: string,
    userId: string,
    blurFactor: number,
  ): Promise<void> {
    return Jimp.read(imgUrl)
      .then((image) => {
        if (blurFactor === 0) {
          image.write(`./images/${userId}/secret.png`);
        } else {
          image
            .blur(blurFactor)
            .color([{ apply: 'hue', params: [blurFactor * 3] }])
            .write(`./images/${userId}/secret.png`);
        }
      })
      .catch(console.log);
  }
}
